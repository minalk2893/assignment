
d3.json('js/data/panel.json', function(panelsArray) {

  var height = 500,width = 500,
  margin={
    left:120,
    right:20,
    top:20,
    bottom:70
  };

  var panelMap = d3.group(panelsArray,d=>d.location);
  var locations = [];
  panelMap.forEach(function(data,key){
    locations.push(key);
  });

  var rowMax = d3.max(panelsArray,function(d){
    return d.row;
  });

  var canvas = d3.select('#allLocations svg')
      .attr('height', height + margin.top + margin.bottom)
      .attr('width' , width + margin.left + margin.right);
  
  var group = canvas.append('g').attr(
    'transform', "translate("+margin.left + ","+margin.top+")"
  );

  var xScale = d3.scaleBand().domain(locations).range([0,width])
            .padding(.2);
  var yScale = d3.scaleLinear()
    .domain(d3.extent(panelsArray,function(data){
      return data.row;
  })).range([0,height]);

  var colorScale = d3.scaleLinear()
      .domain(d3.extent(panelsArray,function(data){
        return data.value;
      }))
      .range(['#0080FF','	#0000FF']);
  
  var barWidth = width/panelMap.size/2;
  var barHeight = height/rowMax;

  var xAxis = d3.axisTop(xScale);
  var yAxis = d3.axisLeft(yScale).ticks(rowMax);
  


  group.selectAll('g').data(panelsArray).enter().append('g').attr(
    'transform',function(data){
      return "translate(10)";
    })
    .append('rect')
        .attr('width',barWidth)
        .attr('height',barHeight)
        .attr('x',function(d,i){
          return xScale(d.location);
        })
        .attr('y',function(d){
          return yScale(d.row);
        })
        .attr('fill',function(d){
          return colorScale(d.value);
        });

  group.append('g')
    .call(xAxis);

  group.append('g')
    .call(yAxis);

  downloadImage('alllocations');

}); // json import