var generatorService = function(){
    var sectionWidth = "30%"
    function generateInputElements(question){
        var enclosingDiv = document.createElement("div");
        enclosingDiv.setAttribute("id",question.id);
        enclosingDiv.classList.add("form-group");
        enclosingDiv.style.display = "flex";
        var elementLabel = document.createElement("label");
        elementLabel.style.width = sectionWidth;
        elementLabel.innerHTML = question.title;
        var inputElement = document.createElement("input");
        inputElement.classList.add("form-control");
        inputElement.style.width = "50%";
        inputElement.setAttribute("type",question.type);
        if(question.mandatory){
            inputElement.setAttribute("required",true);
        }
        
        enclosingDiv.appendChild(elementLabel);
        enclosingDiv.appendChild(inputElement);
        return enclosingDiv;
    }

    function generateRadioElements(question){
        var enclosingDiv = document.createElement("div");
        enclosingDiv.setAttribute("id",question.id);
        enclosingDiv.classList.add("form-group");
        enclosingDiv.style.display = "flex";
        var elementLabel = document.createElement("label");
        elementLabel.style.width = sectionWidth;
        elementLabel.innerHTML = question.title;
        enclosingDiv.appendChild(elementLabel);
        var breakTag = document.createElement("br");
        enclosingDiv.appendChild(breakTag);
        var radioDiv = document.createElement("div");
        radioDiv.style.display = "flex";
        radioDiv.style.flexDirection = "column";
        for(let index=0;index<question.options.length;index++){
            var radioEleDiv = document.createElement("div");
            var radioLabel = document.createElement("label");
            radioLabel.innerHTML = question.options[index].label;
            var inputElement = document.createElement("input");
            inputElement.classList.add("radio-inline");
            inputElement.setAttribute("type","radio");
            inputElement.setAttribute("name",question.id);
            inputElement.setAttribute("value",question.options[index].value)
            if(question.mandatory){
                inputElement.setAttribute("required",true);
            }
            radioEleDiv.appendChild(inputElement);
            radioEleDiv.appendChild(radioLabel);
            radioDiv.appendChild(radioEleDiv);
        }
        enclosingDiv.appendChild(radioDiv);
        
        return enclosingDiv;
    }

    function generateCheckboxElements(question){
        var enclosingDiv = document.createElement("div");
        enclosingDiv.setAttribute("id",question.id);
        enclosingDiv.classList.add("form-group");
        enclosingDiv.style.display = "flex";
        var elementLabel = document.createElement("label");
        elementLabel.style.width = sectionWidth;
        elementLabel.innerHTML = question.title;
        enclosingDiv.appendChild(elementLabel);
        var checkboxEnclosingDiv = document.createElement("div");
        for(let index=0;index<question.options.length;index++){
            var checkboxDiv = document.createElement("div");
            var checkboxLabel = document.createElement("label");
            checkboxLabel.innerHTML = question.options[index].label;
            var inputElement = document.createElement("input");
            inputElement.classList.add("checkbox-inline");
            inputElement.setAttribute("type","checkbox");
            inputElement.setAttribute("name",question.id);
            inputElement.setAttribute("value",question.options[index].value)
            if(question.mandatory){
                inputElement.setAttribute("required",true);
            }
            checkboxDiv.appendChild(inputElement);
            checkboxDiv.appendChild(checkboxLabel);
            checkboxEnclosingDiv.appendChild(checkboxDiv);
        }
        enclosingDiv.appendChild(checkboxEnclosingDiv);
        return enclosingDiv;
    }

    function generateTextAreaElement(question){
        var enclosingDiv = document.createElement("div");
        enclosingDiv.setAttribute("id",question.id);
        enclosingDiv.classList.add("form-group");
        enclosingDiv.style.display = "flex";

        var elementLabel = document.createElement("label");
        elementLabel.style.width = sectionWidth;
        elementLabel.innerHTML = question.title;
        var textareaElement = document.createElement("textarea");
        textareaElement.classList.add("form-control");
        textareaElement.style.width = "50%";
        textareaElement.setAttribute("type",question.type);
        if(question.mandatory){
            textareaElement.setAttribute("required",true);
        }
        enclosingDiv.appendChild(elementLabel);
        enclosingDiv.appendChild(textareaElement);
        return enclosingDiv;
    }

    function generateRatingElement(question){
        var enclosingDiv = document.createElement("div");
        enclosingDiv.setAttribute("id",question.id);
        enclosingDiv.classList.add("form-group");
        enclosingDiv.style.display = "flex";
        var elementLabel = document.createElement("label");
        elementLabel.style.width = sectionWidth;
        elementLabel.innerHTML = question.title;
        ratingbox.createRatingBox();
        var ratingElement = document.createElement("x-rating");
        ratingElement.style.width = "50%";
        if(question.mandatory){
            ratingElement.setAttribute("data-required",true);
        }
        enclosingDiv.appendChild(elementLabel);
        enclosingDiv.appendChild(ratingElement);
        return enclosingDiv;
    }

    return {
        generateInputElements: generateInputElements,
        generateCheckboxElements:generateCheckboxElements,
        generateRadioElements:generateRadioElements,
        generateTextAreaElement:generateTextAreaElement,
        generateRatingElement:generateRatingElement
    }

}();