
d3.json('js/data/panel.json', function(panelsArray) {

    var height = 500,width = 1000,
    margin={
      left:120,
      right:20,
      top:20,
      bottom:70
    };
  
    var panelMap = d3.group(panelsArray,d=>d.location);
    var locations = [];
    panelMap.forEach(function(data,key){
      locations.push(key);
    });
    var selectedLocation = locations[0];
    d3.select("#specificLocation #selectLocation")
        .selectAll('option')
        .data(locations)
        .enter().append('option')
        .text(function(d){
            return d;
        });
    
    d3.select("#specificLocation #selectLocation").on('change',function(){
        let selectedLocation = d3.select('#selectLocation').property('value');
        generateHeatMap(panelMap.get(selectedLocation));
    });


    generateHeatMap(panelMap.get(selectedLocation));
  
    function generateHeatMap(locationMap){
        d3.selectAll("#specificLocation svg > *").remove();

         var canvas = d3.select('#specificLocation svg')
            .attr('height', height + margin.top + margin.bottom)
            .attr('width' , width + margin.left + margin.right);
    
        var group = canvas.append('g').attr(
            'transform', "translate("+margin.left + ","+margin.top+")"
        );

        var submetricsMax = d3.max(locationMap,function(d){
            return d['sub-values'].length;
        });
        var rowMax = d3.max(locationMap,function(d){
            return d.row;
        });
        var submetricsList = [];
        for(var outerIndex in locationMap){
            var innerMetrics = locationMap[outerIndex]['sub-values'];
            for(var innerIndex in innerMetrics){
                var key = Object.keys(innerMetrics[innerIndex])[0];
                submetricsList.push({
                    row:locationMap[outerIndex].row,
                    index:innerIndex,
                    param: key,
                    paramValue: innerMetrics[innerIndex][key]
                });
            }
        }

        var xScale = d3.scaleLinear()
            .domain([0,submetricsMax])
            .range([0,width]);
        var yScale = d3.scaleLinear()
            .domain(d3.extent(locationMap,function(data){
                return data.row;
            }))
            .range([0,height]);
    
        var colorScale = d3.scaleOrdinal()
            .domain(['low','medium','high'])
            .range(['#ff8000','#ff4000','#ff0000']);
        
        var barWidth = width/submetricsMax;
        var barHeight = height/rowMax;
    
        var yAxis = d3.axisLeft(yScale).ticks(rowMax);
        
    
    
        var rectGrp = group.selectAll('g').data(submetricsList).enter().append('g').attr(
            'transform',function(data){
                return "translate(10)";
            })
            .style('margin-right','15px');
        rectGrp
            .append('rect')
            .attr('width',barWidth)
            .attr('height',barHeight)
            .attr('x',function(d){
                return xScale(d.index);
            })
            .attr('y',function(d){
                return yScale(d.row);
            })
            .attr('fill',function(d){
                return colorScale(d.paramValue);
            });

        rectGrp
            .append('text')
            .text((function(d){
                return d.param;
            }))
            .attr('x',function(d){
                return xScale(d.index)+barWidth/3;
            })
            .attr('y',function(d){
                return yScale(d.row)+barHeight/2;
            })
            .style('fill','white')
            .style('font-size','12px');
            
    
        group
            .append('g')
            .call(yAxis);

        downloadImage('dowloadspecificLocation');
    }
  
  }); // json import