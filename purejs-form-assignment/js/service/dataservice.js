var dataService = function(){
    function getData(method, url){
       return new Promise(function(resolve,reject){
            var xhr = new XMLHttpRequest();
            xhr.open(method, url, true);
            xhr.onload = function(){
                if(this.status == 200){
                    resolve(xhr.response);
                }else{
                    reject({
                        status:this.status,
                        statusText: xhr.statusText
                    });
                }
            }

            xhr.onerror = function(){
                reject({
                    status:this.status,
                    statusText: xhr.statusText
                })
            }

            xhr.send();
       });
    }

    return {
        getData: getData
    }
}();