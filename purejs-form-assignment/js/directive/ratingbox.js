var ratingbox = function(){
    var ratingArray = [0,1,2,3,4,5,6,7,8,9,10];
    var currentSelected = null;
    var isSelected = false;
    createRatingBox = function(){
        var ratingElement = Object.create(HTMLElement.prototype);
        ratingElement.createdCallback = function(){
            var shadow = this.createShadowRoot();

            var ratingTable = document.createElement("table");
            var tableRow = document.createElement("tr");
            for(let index=0;index<ratingArray.length;index++){ 
                let tableCol = document.createElement("th");
                tableCol.style.border = "1px solid black";
                tableCol.width = "50px";
                tableCol.innerHTML = ratingArray[index];
                tableCol.onclick = (event)=>{
                    if(isSelected){
                        currentSelected.style.color = "black";
                        currentSelected.style.backgroundColor = "white";
                    }
                    tableCol.style.color = "white";
                    tableCol.style.backgroundColor = "black";
                    currentSelected = tableCol;
                    this.setAttribute("data-value",index);
                    isSelected = true;
                }
                tableRow.appendChild(tableCol);
            }
            ratingTable.appendChild(tableRow);

            shadow.appendChild(ratingTable);
        }

        document.registerElement('x-rating',{
            prototype:ratingElement
        });
        return ratingElement;
    }

    return {
        createRatingBox:createRatingBox
    }
}();