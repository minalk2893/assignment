generateForm = function(){
    var formHandle = document.getElementById("formHandle");
    var submitButton = document.getElementById("submitButton");
    dataService.getData('GET','/questions').then(
        function(response){
            var questions = JSON.parse(response);
            questions.forEach(function(question){
                if(question.has_options){
                    dataService.getData('GET','/options?question='+question.id).then(
                        function(response){
                            question.options = JSON.parse(response);
                            let count = 1;
                            while(document.getElementById(Number(question.id)+count) == null && count <= questions.length){
                                count++;
                            }
                            if(count > questions.length){
                                formHandle.insertBefore(generateFormElements(question),submitButton);
                            }else{
                                formHandle.insertBefore(generateFormElements(question),document.getElementById(Number(question.id)+count));
                            }
                        }
                    ).catch(function(error){
                        console.log(error);
                    });
                }else{
                    formHandle.insertBefore(generateFormElements(question),submitButton);
                }
            });
        }
    ).catch(function(error){
        console.log(error);
    });
}

function generateFormElements(question){
    switch(question.type){
        case 'text':
        case 'number':
        case 'email':
            return generatorService.generateInputElements(question);
        case 'radio':
            return generatorService.generateRadioElements(question);
        case 'checkbox':
            return generatorService.generateCheckboxElements(question);
        case 'textarea':
            return generatorService.generateTextAreaElement(question);
        case 'rating':
            return generatorService.generateRatingElement(question);
    }
}

validateForm = function(){
    var formHandle = document.getElementById("formHandle");
    var formValid = true;

    var textInputs = document.querySelectorAll("input[type='text'][required]");
    for(let index = 0;index<textInputs.length;index++){
        if(textInputs[index].value == ""){
            formValid = false;
        }
    }

    var emailInputs = document.querySelectorAll("input[type='email'][required]");
    for(let index = 0;index<emailInputs.length;index++){
        if(emailInputs[index].value == ""){
            formValid = false;
        }
    }

    var numberInputs = document.querySelectorAll("input[type='number'][required]");
    for(let index = 0;index<numberInputs.length;index++){
        if(numberInputs[index].value == ""){
            formValid = false;
        }
    }

    var textareaInputs = document.querySelectorAll("textarea[required]");
    for(let index = 0;index<textareaInputs.length;index++){
        if(textareaInputs[index].value == ""){
            formValid = false;
        }
    }

    var radioInputs = document.querySelectorAll("input[type='radio'][required]");
    let radioValid = false;
    for(let index = 0;index<radioInputs.length;index++){
        if(radioInputs[index].checked){
            radioValid = true;
            break;
        }
    }
    if(!radioValid && radioInputs.length > 0){
        formValid = false;
    }


    var checkedInputs = document.querySelectorAll("input[type='checkbox'][required]");
    let checkboxValid = false;
    for(let index = 0;index<checkedInputs.length;index++){
        if(checkedInputs[index].checked){
            checkboxValid = true;
            break;
        }
    }
    if(!checkboxValid && checkedInputs.length > 0){
        formValid = false;
    }

    var ratingInputs = document.querySelectorAll("x-rating[data-required='true']");
    for(let index = 0;index<ratingInputs.length;index++){
        if(ratingInputs[index].dataset.value == undefined || ratingInputs[index].dataset.value == ""){
            formValid = false;
        }
    }

    if(formValid){
        formHandle.style.display = 'none';
        alert("FormSubmitted Successfully");
    }else{
        alert("Please fill all the details");
        return false;
    }
}