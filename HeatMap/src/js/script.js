function getDownloadURL(svg, callback) {
    var canvas;
    var width = 1000,height=500;
    var source = svg.parentNode.innerHTML;
    var image = d3.select('body').append('img')
      .style('display', 'none')
      .attr('width', width)
      .attr('height', height)
      .node();
  
    image.onerror = function() {
      callback(new Error('An error occurred while attempting to load SVG'));
    };
    image.onload = function() {
      canvas = d3.select('body').append('canvas')
        .style('display', 'none')
        .attr('width', width)
        .attr('height', height)
        .node();
  
      var ctx = canvas.getContext('2d');
      ctx.drawImage(image, 0, 0);
      var url = canvas.toDataURL('image/png');
  
      d3.selectAll([ canvas, image ]).remove();
  
      callback(null, url);
    };
    image.src = 'data:image/svg+xml,' + encodeURIComponent(source);
  }
  
  function updateDownloadURL(svg, link) {
    getDownloadURL(svg, function(error, url) {
      if (error) {
        console.error(error);
      } else {
        link.href = url;
      }
    });
  }
  
  

  downloadImage = function(image){
      if(image == 'alllocations'){
        var svg = d3.select('#allLocations svg');
        updateDownloadURL(svg.node(), document.getElementById('dowloadAllLocations'));
      }else if(image == 'dowloadspecificLocation'){
        var svg = d3.select('#specificLocation svg');
        updateDownloadURL(svg.node(), document.getElementById('dowloadspecificLocation'));
      }
  }